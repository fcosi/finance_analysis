# Financial Data collector
***
Collects financial data of companies online and analyses them


### Example
***
As a watchlist for the basic `run_scraper_watchlist.py` use the following syntax inside a `watchlist.txt` file:

```
companynane:COMPANY.ID
```

The company name is arbitrary, the Company-id has to be the ID used by finance.yahoo.com
example for some random companies:

```
Orsted:ORSTED.CO
Amazon:AMZN
Google:GOOG
```

# Contributers

The original authors of this package are:

- Filippo G. Cosi
