from scrapers import YahooScraper
import pandas as pd

if __name__ == "__main__":
    companies = pd.read_csv("watchlist.txt", ":", header=None)

    scraper = YahooScraper()
    scraper.load_full_watchlist(companies)
    scraper.run_all()
