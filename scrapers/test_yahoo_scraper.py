import os
import shutil
from unittest import TestCase

from scrapers import YahooScraper


class TestYahooScraper(TestCase):
    def setUp(self) -> None:
        self.scraper = YahooScraper()

    def test_parse_statistics(self):
        statistics = self.scraper.parse_statistics("GOOG")

        self.assertIsInstance(statistics, dict)
        self.assertTrue("Market Cap (intraday) 5" in statistics.keys())

    def test_save_statistics(self):
        statistics = self.scraper.parse_statistics("GOOG")
        self.scraper.save_statistics(company_id="GOOG", statistics=statistics)
        path = "./data/GOOG"

        self.assertTrue(os.path.exists(path))

        # clean up directory
        shutil.rmtree(path)
