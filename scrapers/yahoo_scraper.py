import os
from typing import Dict
import pandas as pd


class YahooScraper:
    BASE_URL = "https://finance.yahoo.com"

    def __init__(self):
        pass

    def load_full_watchlist(self, watchlist: Dict[str, str]) -> None:
        self.watchlist = watchlist

    def parse_statistics(self, company_short_id) -> Dict[str, pd.DataFrame]:
        link = f"{self.BASE_URL}/quote/{company_short_id}/key-statistics/?p={company_short_id}"

        # returns list of DFs
        stats = pd.read_html(link)
        statistics = {}
        for stat in stats:
            statistics.update({stat[0][0]: stat})

        return statistics

    @staticmethod
    def save_statistics(
        company_id: str,
        statistics: Dict[str, pd.DataFrame],
        company_name: str = None,
    ) -> None:
        saving_folder = company_id
        if company_name is not None:
            saving_folder = company_name

        rel_path = f"./data/{saving_folder}"
        os.makedirs(rel_path, exist_ok=True)

        for key, val in statistics.items():
            val.to_csv(f"{rel_path}/{key}", index=False)

    def save_all(self, company_id: str, company_name: str = None) -> None:
        self.save_statistics(
            company_id=company_id,
            statistics=self.parse_statistics(company_id),
            company_name=company_name,
        )

    def run_all(self) -> None:
        # TODO check date
        if self.watchlist is None:
            raise Exception

        for key, val in zip(self.watchlist[0], self.watchlist[1]):
            self.company_short_id = val
            self.save_all(company_id=val, company_name=key)
